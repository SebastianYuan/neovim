" Setting for indentLine Plugin
" Enable indent line guide
let g:indentLine_concealcursor='inc'
let g:indentLine_fileTypeExclude=['tex', 'json']
let g:indentLine_char='▏'
let g:indentLine_setColors=1
let g:indentLine_setConceal=0

" Airline Theme
let g:airline_theme='tomorrow'

" Airline setup "
let g:airline#extension#branch#enable=1
let g:airline_powerline_fonts=1
let g:airline#extensions#branch#vcs_priority=["git", "mercurial"]
let g:airline#extensions#branch#displayed_head_limit=10
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 1
let g:airline#extensions#tabline#left_sep = ""
let g:airline#extensions#tabline#left_alt_sep = "|"


" Setting for split terminal
" Terminal Function
let g:term_buf = 0
let g:term_win = 0
function! TermToggle(height)
    if win_gotoid(g:term_win)
        hide
    else
        botright new
        exec "resize " . a:height
        try
            exec "buffer " . g:term_buf
        catch
            call termopen($SHELL, {"detach": 0})
            let g:term_buf = bufnr("")
            set nonumber
            set norelativenumber
            set signcolumn=no
        endtry
        startinsert!
        let g:term_win = win_getid()
    endif
endfunction

" Toggle terminal on/off (neovim)
nnoremap <C-j> :call TermToggle(12)<CR>
inoremap <C-j> <Esc>:call TermToggle(12)<CR>
tnoremap <C-j> <C-\><C-n>:call TermToggle(12)<CR>

" Python Format on Save with Black
autocmd BufWritePre *.py execute ':Black'
