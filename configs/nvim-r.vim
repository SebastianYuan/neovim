vmap <C-Space> <Plug>RDSendSelection
nmap <C-Space> <Plug>RDSendLine
" R output is highlighted with current colorscheme
let g:rout_follow_colorscheme = 1
" R commands in R output are highlighted
let g:Rout_more_colors = 1
let R_editor_w = 80
let R_editor_h = 60
let R_nvimpager="horizontal"
" press -- to have Nvim-R insert the assignment operator: <-
let R_assign_map = "--"

" set a minimum source editor width
let R_min_editor_width = 80

" make sure the console is at the bottom by making it really wide
let R_rconsole_width = 1000

" show arguments for functions during omnicompletion
let R_show_args = 1

" Don't expand a dataframe to show columns by default
let R_objbr_opendf = 0
"autocmd VimResized * let R_rconsole_width = winwidth(0) / 2
