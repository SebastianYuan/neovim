" Use ; as an alias for : in Normal mode
nnoremap ; :
imap jj <esc>

" Toggle NerdTree
nmap <C-b> :NERDTreeToggle<CR>

" Buffer Setting
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprev<CR>
nnoremap <C-x> :bd<CR>

" Terminal go back to normal mode
tnoremap <Esc> <C-\><C-n>
tnoremap jj <C-\><C-n>
tnoremap :q! <C-\><C-n>:q!<CR>
