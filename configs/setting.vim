" Colors
if (has("nvim"))
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif
if (has("termguicolors"))
    set termguicolors
endif

" Themes
silent! colorscheme onedark
let g:onedark_termcolors=16
let g:onedark_terminal_italics=1 " enable italic fontface

syntax on     " enable sytax processing

" Fixes backspace
set backspace=indent,eol,start

" Set number of lines of history vim has to keep
set history=500

" Enable filetype plugins
filetype plugin on
filetype indent on

" Set Unix file type
set ffs=unix,dos,mac

" locale and encoding
let $LANG='en'
set langmenu=en
set encoding=utf8

" Set to auto-read when a file is modified elsewhere
set autoread

" highlight cursor line
set cursorline

" Deinfe ruler
set colorcolumn=81,101

" Spaces and Tabs
set tabstop=4     " number of visual space per Tab
set softtabstop=4 " number of spaces in tab when editing
set shiftwidth=4  " number of spaces to use for autoindent
set expandtab     " spaces as tab
set autoindent    " auto indent
set smartindent   " smart indent
set copyindent    " conpy indent from the previous line
set smarttab      " be smart when using tabs

" disable markdown conseal
let g:markdown_syntax_conceal=0

" Searches
set incsearch
set nohlsearch
set ignorecase
set smartcase

" Turn on regex
set magic

" Cursors setup
set guicursor=n-v:block,i-c-ci-ve:hor20,r-cr:ver25,o:hor50
au VimLeave * set guicursor=a:hor20-blink1

" Set 7 lines to the moving cursor
set so=7

" Turn backup off (just use VCS: git, svn, etc)
set nobackup
set nowb
set noswapfile

" Italic font
set t_ZH=^[[3m
set t_ZR=^[[23m
" Italic font for comments
highlight Comment cterm=italic

" auto remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

"""""""""""""""
" Auto command
"""""""""""""""
" Automatic toggling between line number modes
set number relativenumber
augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
augroup END

