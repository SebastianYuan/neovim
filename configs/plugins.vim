"""""""""""""""""
" Vim-Plug Setup
"""""""""""""""""
if has('win32') || has('win64')
    call plug#begin('$HOME\AppData\Local\nvim\bundles')
else
    call plug#begin('~/.config/nvim/bundles')
endif
""""""""""""""""""""""""""""""""""""""""""""
" Basic functionality and apperance plugins
""""""""""""""""""""""""""""""""""""""""""""
Plug 'scrooloose/nerdtree'              " Tree-like text-based Directories
Plug 'scrooloose/nerdcommenter'
Plug 'vim-airline/vim-airline'          " Horizontal Status Bar Plugin
Plug 'vim-airline/vim-airline-themes'   " Enable theme editing for the status bar
Plug 'joshdick/onedark.vim'             " Atom One Dark Colourscheme for Neovim
Plug 'luochen1990/rainbow'              " Colorized Parentheses for Neovim
Plug 'tpope/vim-surround'               " Enable surround changes
Plug 'tpope/vim-fugitive'               " Enable git status in airline for Neovim
Plug 'junegunn/vim-easy-align'          " Align operators such as =, +=, *=, etc
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
"""""""""""""""""""""""""""""
" Language extension plugins
"""""""""""""""""""""""""""""
Plug 'JuliaEditorSupport/julia-vim' " Added support for Julia Language
Plug 'jalvesaq/Nvim-R'              " R IDE setup for Neovim
Plug 'sheerun/vim-polyglot'         " extended syntax supports in Neovim
Plug 'Yggdroot/indentLine'          " Indent Guides plugin for Neovim
""""""""""""""""""""""""""""
" Web Development Plugins
""""""""""""""""""""""""""""
Plug 'mattn/emmet-vim'              " Enable emmet in vim
"""""""""""""""""""""""""""""""""""
" Intelligent completion Framework
"""""""""""""""""""""""""""""""""""
" NCM 2 plugin
Plug 'ncm2/ncm2'                " Async completion framework for neovim
Plug 'roxma/nvim-yarp'          " remote update plugin
" NCM 2 sources' plugins
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'HansPinckaers/ncm2-jedi'  " Python Autocompletion
Plug 'ncm2/ncm2-vim'
Plug 'gaalcaras/ncm-R'
""""""""""""""""""""""
" Python Plugin
""""""""""""""""""""""
Plug 'psf/black', {'tag': '*'}        " Python Black Formatter
" call PlugInstall to install new plugins
call plug#end()
