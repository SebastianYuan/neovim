## Setup for this neovim configs
### Before cloning, please make sure neovim is installed and PATH is set up

Please note that this neovim setup require modern font
List of recommanded font face
 - FiraCode
 - Iosevka
 - Dank Mono (My Personal Favourite)
 - Menlo

```bash
#This path is for MacOS or Linux
git clone https://gitlab.com/SebastianYuan/neovim.git ~/.config/nvim
#This is for windows
git clone https://gitlab.com/SebastianYuan/neovim.git $HOME\AppData\Local\nvim

# To ensure all plugins are installed
nvim -c PluginInstall
```
```bash
# To ensure properly working with 'conda'
# make sure the jedi, neovim, and black are installed in your environment
# for example
conda create -n myenv python=3.8 numpy pandas scipy
conda install -n myenv neovim jedi black -y
```


